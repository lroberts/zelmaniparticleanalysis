import h5py 
import numpy as np
import matplotlib.pyplot as plt
import pylab  

fin = h5py.File("tracers_combine.h5","r")

times = fin["/Time"]
rho = fin["/Density"]
entropy = fin["/Entropy"]
KE = fin["/KineticEnergyAtInfinity"]
indices = [] 
for (i,ke) in enumerate(KE[-1,:]):
	if ke>0.0:
		print i,ke
		indices.append(i) 
plt.clf()
for i in indices:
	plt.semilogy(times,rho[:,i]) 
plt.xlabel('Time') 
plt.ylabel('Density') 
plt.savefig('Density.pdf') 

plt.clf()
for i in indices:
	plt.semilogy(times,entropy[:,i]) 
plt.xlabel('Time') 
plt.ylabel('Entropy') 
plt.savefig('Entropy.pdf') 
  
plt.clf()
for i in indices:
	plt.plot(times,KE[:,i]) 
plt.xlabel('Time') 
plt.ylabel('KEInfinity') 
plt.savefig('KEInfinity.pdf') 
  
 
