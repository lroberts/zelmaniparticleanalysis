import h5py 
import numpy as np 
import sys 
import glob 

verbose = True

base_dir = "/panfs/ds06/sxs/bernuzzi/simulations/LS220_M135135_D40/output-*/data/tracers*.hdf5" 
files = glob.glob(base_dir)
 
var_types = []
itermin   = -1
itermax   = -1
nparticle = 0

# Get the datasets in the file 
fin = h5py.File(files[0],"r") 

file_keys = [] 
base_group = "PartType0/"
fin[base_group].visit(file_keys.append) 
nparticle = len(fin[base_group+file_keys[0]])
dtype = fin[base_group+file_keys[0]].dtype  
var_types = file_keys 

fin.close() 

# Get the times for files and order files
times = []
for file in files:
	try: 
		fin = h5py.File(file,"r") 
		times.append(float(fin["/Header/"].attrs['Time'])) 
		fin.close()
	except:
		files.remove(file)
niter = len(times)  
	 
# Sort the files by time 
files = [x for (y,x) in sorted(zip(times,files))]
times = sorted(times)  
# Debug check to make sure times are increasing
#def strictly_increasing(L): 
#	return all(x<=y for x, y in zip(L, L[1:])) 
# print strictly_increasing(times) 

# Alert the user to the information found  
if verbose: 
	print "number of particles  : ",nparticle
	print "number of iterations : ",niter

if verbose: 
	for name in var_types:
		print name

# Create the combined data file	
fout = h5py.File("tracers_combine.h5","w") 
fin = h5py.File(files[0],"r") 
dsets = []
fout["/Time"] = times 
for name in var_types:
	dsets.append(fout.create_dataset(name,(niter,)+fin[base_group+name].shape,dtype=dtype)) 
fin.close()

# Combine the particle data in the new file 
for (i,file) in enumerate(files):
	if verbose: print i, file
	fin = h5py.File(file,"r")  
	for name in file_keys:
		fout[name][i,:] = fin[base_group+name][:]  
	fin.close()

if verbose: print "\n"	
	
	
	
	
	
	
	 

